from typing import Callable, Dict, Sequence, Union

from distributed import Client, Future, as_completed
from progressbar import AdaptiveETA, Bar, Percentage, ProgressBar

pb_widgets = [
    'Progress: ', Percentage(),
    ' - ',
    ' ', Bar(marker='█'),
    ' ', AdaptiveETA()
]


def exec_(schedule: Sequence[Dict],
          client: Client = None,
          apply_for=None,
          globals_=None):
    """Executes a schedule of tasks grouped in steps. The scheduler
    uses ``dask`` to distribute de load among the processes in the
    cluster.

    # TODO: Improve these docs.
    :param schedule: The schedule: a sequence of steps that describe
        a series of tasks.
    :param client: The client that submits the load among the processes
        of the cluster.
    :param apply_for: A initial data or future. The initial step load
        is submitted with this data/future. If a sequence is given, the
        step will be submitted along each element of this sequence.
    :param globals_: A set of global data/futures. They will be passed
        as data for every task submitted.
    :return:
    """
    schedule = iter(schedule)

    ini_load = next(schedule)
    futures_load, size_load, load_order = \
        submit_step_load(client, ini_load, apply_for=apply_for,
                         globals_=globals_)
    futures, size, order = futures_load, size_load, load_order

    while True:
        try:
            step_load = next(schedule)
            futures, size, order = \
                submit_step_load(client, step_load, futures, globals_)
            futures_load.extend(futures)

        except StopIteration:
            results, order_map = [], {}
            non_last = size_load - size
            pb = ProgressBar(widgets=pb_widgets)

            pb.start(max_value=size_load)
            completed = as_completed(futures_load)
            for i, future in enumerate(completed):
                if future.key in order:
                    fr = future.result()
                    results.append(fr)
                    order_map[future.key] = i - non_last

                pb.update(i + 1)

            pb.finish()
            break

        else:
            size_load += size

    # Return results in the order the tasks were submitted.
    return [results[order_map[k]] for k in order]


def submit_step_load(client: Client,
                     step_load: Dict,
                     apply_for: Union[
                         Future, Sequence[Future]
                     ] = None,
                     globals_: Sequence[Future] = None):
    """

    :param client: The client that submits the load among the processes
        of the cluster.
    :param step_load: A mapping with the description of the task.
    :param apply_for: A initial data or future. The initial step load
        is submitted with this data/future. If a sequence is given, the
        step will be submitted for each element of this sequence.
    :param globals_: A set of global data/futures. They will be passed
        as data for every task submitted.
    :return: A collection of the futures submitted to the cluster, as
        as well as the size of the load and the order of submission.
    """
    if not isinstance(apply_for, Sequence):
        return submit_step_load(client, step_load, [apply_for], globals_)

    fn = step_load['callable']
    data = step_load['args']
    distribute = step_load.get('distribute', False)

    globals_ = globals_ or ()

    # Whether to distribute the whole data or element by element
    data = (data,) if not distribute else data

    futures, size, order = [], 0, []
    for i, ef in enumerate(apply_for):
        for j, d in enumerate(data):
            sd = client.scatter(d)
            task = client.submit(fn, sd, ef, *globals_)
            futures.append(task)
            order.append(task.key)
            size += 1

    return futures, size, order


def gather_results(futures: Sequence[Future],
                   exclude: Callable = None,
                   size: int = None,
                   count_offset: int = None):
    """Collect the results from a collection of futures
    submitted to a cluster.

    :param futures: A collection of `Futures`.
    :param exclude: A callable applied to every future. It true, that
        particular future will be excluded from the results.
    :param size: The size of the futures sequence. Optional. If not given
        it is the length of ``futures``.
    :param count_offset: An offset to get the order of a future result as
        it is completed.
    :return: The results of the non-excluded futures, as well as a mapping
        with the order in which the results got completed.
    """
    size = size or len(futures)
    results, order_map = [], {}
    pb = ProgressBar(widgets=pb_widgets)

    pb.start(max_value=size)
    completed = as_completed(futures)
    for i, future in enumerate(completed):
        if exclude(future):
            continue
        fr = future.result()
        results.append(fr)
        order_map[future.key] = i - count_offset
        pb.update(i + 1)

    pb.finish()

    return results, order_map
