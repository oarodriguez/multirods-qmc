from math import pi

# Energy units
UE = 1.  # Reference energy
ER = pi ** 2 * UE  # Recoil energy

# Length units
LKP = 1.  # Kronig-Penney potential period.
K_OPT = pi / LKP  # Optical momentum of the lattice.
