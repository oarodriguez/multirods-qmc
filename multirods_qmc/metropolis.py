import math
from multiprocessing import current_process
from time import time
from typing import Callable

import numpy as np
import numpy.random as random
from numba import jit

VAL_SLOT = 0
SQR_SLOT = 1

MAIN_SLOT = 0
AUX_SLOT = 1

UNIFORM_MOVE = 0
GRADIENT_DESCENT_MOVE = 1


def allocate_conf(*, space_size):
    """

    :param space_size:
    :return:
    """
    return np.zeros((space_size, 2, 2), dtype=np.float64)


def jit_pdf_sampler(pdf_fn: Callable,
                    advance_fn: Callable, *,
                    space_size: int) -> Callable:
    """Builds a JIT compiled function that executes a Quantum Monte Carlo
    integration over a many body quantum fluid.

    :param pdf_fn: The wave function used as probability distribution
        function (distribution).
    :param advance_fn: The function that displaces the particles in the
        configuration space.
    :param space_size: The dimension of the configuration space where the
        integration is going to be realized.
    :return: The JIT compiled function that execute the Monte Carlo
        integration.
    """
    pdf = pdf_fn
    ss = space_size

    @jit(nopython=True)
    def sampler_kernel(distribution_params,
                       displace_params,
                       z_drift_conf,
                       samples, transient_samples,
                       burnout_samples,
                       seed=False):
        """Samples the probability distribution function
        :meth:`distribution_function` using the Metropolis-Hastings
        algorithm.

        :param distribution_params:
        :param displace_params:
        :param z_drift_conf: The buffer to store the positions and
                             drift velocities of the particles. It
                             is assumed that it contains the initial
                             configuration of the particles.
        :param samples:
        :param transient_samples:
        :param burnout_samples:
        :param seed:
        :return:
        """
        # Do not allow invalid parameters.
        assert burnout_samples >= 0
        assert transient_samples >= 1

        # Avoid `Untyped global name error` when executing the code in a
        # multiprocessing pool.
        rand = random.rand
        log = math.log

        # Feed the numba random number generator with the given seed.
        if seed:
            random.seed(seed)

        # TODO: What is better: allocate or pass a pre-allocated buffer?
        mcs = (samples + 1, ss, 2)
        # z_drift_conf = np.zeros((ss, 2, 2), dtype=np.float64)

        # TODO: store multiplicity of a given configuration.
        # When a random move is rejected, is unnecessary to store the
        # configuration data once more.
        markov_chain = np.zeros(mcs, dtype=np.float64)
        # conf_freq = np.zeros(samples + 1, dtype=np.int32)

        main_conf = z_drift_conf[:, MAIN_SLOT]
        aux_conf = z_drift_conf[:, AUX_SLOT]

        # Initial iteration.
        pdf_actual = pdf(distribution_params, z_drift_conf=main_conf)

        # Initialize accepted move variable.
        accepted_moves = 0

        # The first random moves do not correctly sample the probability
        # distribution function (in principle). They are discarded.
        for idx in range(burnout_samples):

            advance_fn(displace_params, main_conf, aux_conf)
            pdf_proposed = pdf(distribution_params, z_drift_conf=aux_conf)

            # Metropolis condition
            if pdf_proposed > 0.5 * log(rand()) + pdf_actual:
                # Accept the movement
                main_conf[:] = aux_conf[:]
                pdf_actual = pdf_proposed
                accepted_moves += 1

        # Save first value.
        markov_chain[0, :, 0] = main_conf[:, 0]

        # These random moves will actually sample the probability
        # function of the system. They can be used to calculate
        # some physical property.
        for idx in range(1, samples + 1):

            # Transient samples. They are not stored, but only realized
            # in order to reduce the correlation between consecutive
            # samples.
            for jdx in range(transient_samples):

                # NOTICE: Can this section be included in the model kernel?
                advance_fn(displace_params, main_conf, aux_conf)

                pdf_proposed = pdf(distribution_params, z_drift_conf=aux_conf)

                # Accept the movement.
                if pdf_proposed > 0.5 * log(rand()) + pdf_actual:
                    main_conf[:] = aux_conf[:]
                    pdf_actual = pdf_proposed
                    accepted_moves += 1

            # Store markov chain data.
            markov_chain[idx, :, 0] = main_conf[:, 0]

        # TODO: Should we account burnout and transient moves?
        accept_rate = (accepted_moves / (
            burnout_samples + transient_samples * samples
        ))

        return markov_chain, accept_rate

    return sampler_kernel


@jit(nopython=True)
def eval_drift(drift_parts, drift_buffer):
    """

    :param drift_parts:
    :param drift_buffer:
    :return:
    """
    # Number of particles goes here.
    nss = drift_parts.shape[0]

    for idx in range(nss):
        #
        drift_idx = 0.

        one_body_fn_idx_lnd = drift_parts[idx, idx]

        # One body term contribution.
        drift_idx += one_body_fn_idx_lnd

        for jdx in range(nss):
            if jdx == idx:
                # Diagonal terms do not contribute.
                continue

            # Two-body term contributions.
            two_body_fn_idx_jdx_lnd = drift_parts[idx, jdx]
            drift_idx += two_body_fn_idx_jdx_lnd

        drift_buffer[idx] = drift_idx


def jit_estimator(property_fn,
                  property_shape):
    """

    :param property_fn:
    :param property_shape:
    :return:
    """

    @jit(nopython=True)
    def integrator_kernel(property_params,
                          pdf_sample,
                          tolerance,
                          keep_markov_chain=False):
        """

        :param property_params:
        :param pdf_sample:
        :param tolerance:
        :param keep_markov_chain:
        :return:
        """
        ns = pdf_sample.shape[0]

        if keep_markov_chain:
            mcs = (ns + 1,) + property_shape
        else:
            mcs = (2,) + property_shape

        property_buffer = np.zeros(property_shape, dtype=np.float64)
        estimator_chain = np.zeros(mcs, dtype=np.float64)

        for idx in range(ns):

            z_drift_conf = pdf_sample[idx]
            property_fn(property_params, z_drift_conf, result=property_buffer)

            if keep_markov_chain:
                # Copy property_fn data.
                estimator_chain[idx, :] = property_buffer[:]
            else:
                # Accumulate property_fn data.
                estimator_chain[VAL_SLOT, :] += property_buffer[:]
                estimator_chain[SQR_SLOT, :] += property_buffer[:] ** 2

        if not keep_markov_chain:
            # Return averages.
            estimator_chain[VAL_SLOT, :] /= (ns + 1)
            estimator_chain[SQR_SLOT, :] /= (ns + 1)

        return estimator_chain

    return integrator_kernel


class Sampler(object):
    """Implements a Monte Carlo simulation over a many-body
    quantum system using Lieb-Liniger two-body trial functions.
    """

    # The maximum number of moves used in the Monte Carlo
    # Integration.
    samples = 25000

    # The number of transient moves used in the Monte Carlo
    # integration.
    transient_samples = 20

    # The number of samples to discard from the beginning of
    # the Markov chain.
    burnout_samples = 5000

    # The minimum relative tolerance to attain at the end of
    # the Monte Carlo integration.
    tolerance = 1e-2

    def __init__(self, pdf_fn,
                 advance_fn,
                 space_size):
        """

        :param pdf_fn: The wave function used as probability
                       distribution function (pdf).
        :param advance_fn:
        :param space_size: The dimension of the configuration
                           space where the integration is going
                           to be realized.
        """
        self.pdf_fn = pdf_fn

        self.advance_fn = advance_fn

        self.space_size = space_size

        self.kernel = self.jit_compile()

    def jit_compile(self):
        """Builds a JIT compiled function that executes a Quantum Monte
        Carlo integration over a many body quantum fluid.

        :return: The JIT compiled function that execute the Monte
                 Carlo integration.
        """
        return jit_pdf_sampler(self.pdf_fn,
                               self.advance_fn,
                               space_size=self.space_size)

    def __call__(self, distribution_params,
                 displace_params,
                 initial_conf,
                 samples=None,
                 transient_samples=None,
                 burnout_samples=None,
                 seed=None):
        """Executes the Monte Carlo quadrature for the current simulation.

        :param distribution_params:
        :param displace_params:
        :param initial_conf:
        :param samples: The maximum number or random moves of the
            random walk.
        :param transient_samples: The number of transient moves before
            proceeding to calculate the local energy
            and other properties.
        :param burnout_samples:
        :param seed:
        :return: A tuple of three values: the mean energy, the mean of the
            energy squared, and the number of accepted random moves.
        """
        samples = samples or self.samples
        transient_samples = transient_samples or self.transient_samples
        burnout_samples = (
            burnout_samples or self.burnout_samples
        )

        if transient_samples < 1:
            raise ValueError(
                'The number of transient samples must be greater '
                'or equal than one.'
            )

        # If no seed is specified, seed the numpy RNG with a process-based
        # integer plus the current time in seconds, so the integer is
        # different for every process when working with the
        # ``multiprocessing`` module.
        if seed is None:
            np.random.seed(
                current_process().pid + int(time())
            )

            # Get a seed for the numba RNG. This will be passed to the
            # compiled quadrature function.
            seed = np.random.randint(0, high=2 ** 24)

        # We have to seed the numpy random number
        # generator.
        np.random.seed(seed)

        # Allocate space to save the configuration of the particles and
        # the drift velocity during the execution of a Metropolis-Hastings
        # random displacement. Also allocate space to save an auxiliary
        # configuration for the Metropolis-Hastings algorithm execution.
        z_drift_conf = allocate_conf(space_size=self.space_size)

        # Copy the values from the initial configuration.
        z_drift_conf[:] = initial_conf[:]

        return self.kernel(distribution_params,
                           displace_params,
                           z_drift_conf,
                           samples, transient_samples,
                           burnout_samples,
                           seed=seed)


class Estimator(object):
    """Evaluates a physical property using over a markov chain sampled
    from a probability distribution function.
    """

    # The minimum relative tolerance to attain at the end of
    # the Monte Carlo integration.
    tolerance = 1e-2

    def __init__(self, property_fn,
                 property_shape):
        """

        :param property_fn: The wave function used as probability
                       distribution function (pdf).
        :param property_shape: The dimension of the configuration
                           space where the integration is going
                           to be realized.
        """
        self.property_fn = property_fn

        self.property_shape = property_shape

        self.kernel = self.jit_compile()

    def jit_compile(self):
        """Builds a JIT compiled function that executes a Quantum Monte
        Carlo integration over a many body quantum fluid.

        :return: The JIT compiled function that execute the Monte
                 Carlo integration.
        """
        return jit_estimator(self.property_fn,
                             self.property_shape)

    def __call__(self, property_params,
                 pdf_sample_chain,
                 tolerance=None,
                 keep_markov_chain=False):
        """Executes the Monte Carlo quadrature for the current simulation.

        :return:
        :param property_params:
        :param pdf_sample_chain:
        :param tolerance:
        :param keep_markov_chain:
        :return: A tuple of three values: the mean energy, the mean of the
            energy squared, and the number of accepted random moves.
        """
        tolerance = tolerance or self.tolerance

        return self.kernel(property_params, pdf_sample_chain,
                           tolerance=tolerance,
                           keep_markov_chain=keep_markov_chain)
