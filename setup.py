"""
MultiRodsQMC
============

MultiRodsQMC is a python library to perform Quantum Monte Carlo
simulations on a 1D Bose gas within a multi-rods structure.

MultiRodsQMC implements the Variational Monte Carlo (VMC) approach, and it
uses the Metropolis-Hastings algorithm to evaluate an approximated value of
several properties of a physical system as the ground state energy and
the structure factor, among other properties.

The library is written in pure python, as it uses
`numba <http://numba.pydata.org/>`_ to just-in-time compile_kernel
performance-critical functions that execute numerical CPU-intensive
calculations. Also it is BSD licensed.
"""
from setuptools import setup

NAME = 'multirods_qmc'
VERSION = '0.1.0'

setup(
    name=NAME,
    version=VERSION,
    url='https://bitbucket.org/oarodriguez/multirods-qmc',
    license='BSD-3',
    author='Omar Abel Rodríguez-López',
    author_email='oarodriguez.mx@gmail.com',
    description='A library to perform Quantum Monte Carlo simulations for '
                'a 1D Bose gas within a multi-rods structure.',
    long_description=__doc__,
    packages=[
        'multirods_qmc',
        'multirods_qmc.kernels'
    ],
    include_package_data=True,
    zip_safe=False,
    platforms='any',
    install_requires=[
        'numpy>=0.10',
        'scipy>=0.17.0',
        'matplotlib>=2.2',
        'numba>=0.35',
        'h5py>=2.5.0',
        'mpmath>=1.0',
        'progressbar2>=3.6.0',
        'pytz>=2016.4',
        'tzlocal>=1.2',
        'dask>=0.17',
        'distributed>=1.2',
        'PyYAML>=3.10',
        'jinja2>=2.10'
    ],
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Environment :: Console',
        'Intended Audience :: Developers',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Topic :: Scientific/Engineering :: Physics',
        'Topic :: Software Development :: Libraries :: Python Modules'
    ]
)

VERSION = '0.1dev1'
