# MultiRodsQMC

A library to perform Quantum Monte Carlo simulations for many-body 
quantum systems. It's written in pure Python, as it uses [Numba][1] for
performance-critical functions that execute CPU-intensive calculations.

The library is released under the BSD-3 License.


## Authors

**Omar Abel Rodríguez López**

- Github profile: [https://github.com/oarodriguez][ghp]
- Bitbucket profile: [https://bitbucket.org/oarodriguez][bbp] 


[1]: http://numba.pydata.org/
[2]: https://bitbucket.org/oarodriguez/multirods-qmc
[ghp]: https://github.com/oarodriguez
[bbp]: https://bitbucket.org/oarodriguez
